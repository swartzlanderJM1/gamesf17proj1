﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCRotation : MonoBehaviour {

    private float rotSpeed = 0.3f;
    private float rotDirection;

    // Use this for initialization
    void Start()
    {
        // Create clockwise/counterclockwise initial direction
        rotDirection = Random.Range(-1, 1);
        if (rotDirection >= 0)
        {
            rotDirection = 1;
        }
        else
        {
            rotDirection = -1;
        }
	}
	
	// Update is called once per frame
	void Update () {
        transform.Rotate(Vector3.back * rotDirection * rotSpeed); // Rotate clockwise
    }
}
