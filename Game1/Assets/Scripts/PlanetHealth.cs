﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlanetHealth : MonoBehaviour {

    public GameObject planet;
    public GameObject enemy;
    public Text textPlanetHealth;
    public int planetHealth = 10;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        //maybe put an end game thing here if planet
        // health == 0
        //some sort of explosion sound & graphic????

        //Health on screen is updated
        textPlanetHealth.text = planetHealth.ToString();
	}

    private void OnTriggerEnter(Collider c)
    {
        GameObject o = c.gameObject;
        string nameTag = o.tag;
        AudioSource audioSource = o.GetComponent<AudioSource>();
        if (nameTag == "enemy")
        {
            audioSource.Play();
            Destroy(o.gameObject);
            Debug.Log("Enemy Crashed into planet");
            //need to subtract from planet health
            planetHealth -= 1;
            //update health on screen as well

        }
    }
}
