﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBRotation : MonoBehaviour {

    private float timer = 0;
    private int changePeriod = 2; // How often to change direction
    private float rotSpeed = 0.2f; // Uniform speed to rotate
    private float rotDirection;

	// Use this for initialization
	void Start () {
        // Create clockwise/counterclockwise initial direction
        rotDirection = Random.Range(-1, 1);
        if (rotDirection >= 0)
        {
            rotDirection = 1;
        }
        else
        {
            rotDirection = -1;
        }
    }
	
	// Update is called once per frame
	void Update () {
        if (timer <= changePeriod)
        {
            transform.Rotate(Vector3.back * rotDirection * rotSpeed); // Rotate first direction
        }
        else if (timer > changePeriod && timer <= 2 * changePeriod)
        {
            transform.Rotate(Vector3.back * rotDirection * -1f * rotSpeed); // Rotate opposite direction
        }
        else
        {
            timer = 0;
            transform.Rotate(Vector3.back * rotDirection * rotSpeed); // Rotate first direction
        }
        timer += Time.deltaTime;
    }
}
